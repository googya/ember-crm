App.LeadController = Ember.ObjectController.extend({
  actions: {
    saveChanges: function() {
      if (this.get('model.isDirty')) 
        this.get('model').save();
    },
    
    showUnsavedMessage: function(){
      return this.get('model.isDirty') && !this.get('model.isSaving')
    }.property('isDirty', 'isSaving'), 
    
    delete: function() {
      var self = this;
      this.get('model').destroyRecord().then(function() {
        self.transitionToRoute('leads');
      })
    }
  }
})