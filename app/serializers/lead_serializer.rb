class LeadSerializer < ActiveModel::Serializer
  attributes *%i( id first_name last_name email phone status notes )
end
